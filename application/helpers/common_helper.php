<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('Permalink')) {
    function Permalink($s) {
        $c = array (' ');
        $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');
        $s = str_replace($d, '', $s);
        $s = strtolower(str_replace($c, '-', $s));
        return $s;
    }
}
?>