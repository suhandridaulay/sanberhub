<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryModel extends CI_Model {

	public function all(){
        $this->db->select("*");
        $this->db->order_by('catName','ASC');
        $query = $this->db->get('category');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function getId($id){
        $this->db->where('catId', $id);
        $query = $this->db->get('category');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }


}
