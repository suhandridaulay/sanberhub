<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseModel extends CI_Model {

    public function save($field){
        $this->db->insert('course', $field);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            return $insert_id;
        }else{
            return false;
        }
    }

    public function getId($id){
        $this->db->where('courseId', $id);
        $query = $this->db->get('course');
        if($query->num_rows() > 0){
            return $query->row();
        }else{
            return false;
        }
    }

	public function all(){
        $this->db->select("*");
        $this->db->from('course as a');
        $this->db->order_by('a.courseId','DESC');
        $this->db->join('organization as b','b.orgId=a.orgId','left');
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }

    public function update($field, $id){
        $this->db->where('courseId',$id);
        $this->db->update('course', $field);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    

    public function delete($id){
        $this->db->where('courseId', $id);
        $this->db->delete('course');

        if($this->db->affected_rows() > 0){           
            return true;
        }else{
            return false;
        }
    }


}
