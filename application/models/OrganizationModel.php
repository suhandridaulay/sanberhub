<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrganizationModel extends CI_Model {

	public function all(){
        $this->db->select("*");
        $this->db->order_by('orgName','ASC');
        $query = $this->db->get('organization');
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }


}
