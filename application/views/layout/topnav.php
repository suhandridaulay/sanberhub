<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body p-0">
                <ul class="navbar-right list-inline float-right mb-0">
                    <li class="dropdown notification-list list-inline-item">
                        <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="mdi mdi-bell-outline noti-icon"></i>
                            <span class="badge badge-pill badge-danger noti-icon-badge">4</span>
                        </a>
                        
                    </li>

                    <li class="dropdown notification-list list-inline-item">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="<?php echo base_url('assets/images/avatar.jpeg'); ?>" alt="user" class="rounded-circle">
                            </a>
                            
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</div>