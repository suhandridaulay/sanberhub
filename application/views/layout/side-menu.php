<div class="left side-menu">
                <div class="slimscroll-menu">
                    <div id="sidebar-menu">
                        <ul class="metismenu" id="side-menu">
                            <li>
                                <a href="#" class="waves-effect">
                                    <i class="mdi mdi-home-outline mdi-24px" style="  margin-right: 10px;"></i> Dashboard <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i></span> </span>
                                </a>
                            </li>
                            <li class="menu-title">Clone Management</li>
                            <li class="mm-active">
                                <a href="javascript:void(0);" class="waves-effect mm-active"><i class="mdi mdi-cube-outline mdi-24px" style="margin-right: 10px;"></i> Content <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i> </span></a>
                                <ul class="submenu">
                                    <li><a href="#">Assignment</a></li>
                                    <li><a href="#">Webinar</a></li>
                                    <li><a href="#">Video</a></li>
                                    <li><a href="#">Podcast</a></li>
                                    <li><a href="#">Files</a></li>
                                    <li><a href="#">Certificate</a></li>
                                    <li><a href="<?php echo base_url('course'); ?>" class="mm-active">Course</a></li>
                                    <li><a href="#">Pathways</a></li>
                                    <li><a href="#">Voucher</a></li>
                                    <li><a href="#">Quiz</a></li>
                                    <li><a href="#">Category</a></li>
                                    <li><a href="#">Competencies</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="waves-effect">
                                    <i class="mdi mdi-account-outline mdi-24px" style="margin-right: 10px;"></i> Account <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i></span> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect">
                                    <i class="mdi mdi-cog-outline mdi-24px" style="margin-right: 10px;"></i> Setting <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i></span> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect">
                                    <i class="mdi mdi-chart-pie mdi-24px" style="margin-right: 10px;"></i> Analytic <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i></span> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect">
                                    <i class="mdi mdi-camera-iris mdi-24px" style="margin-right: 10px;"></i> Branding <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i></span> </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="waves-effect">
                                    <i class="mdi mdi-link-variant mdi-24px" style="margin-right: 10px;"></i> Integrasi <span class="float-right menu-arrow"><i class="dripicons-chevron-right"></i></span> </span>
                                </a>
                            </li>
                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>