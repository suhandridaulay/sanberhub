<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title><?php echo $title; ?></title>
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/metismenu.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/icons.min.css'); ?>" rel="stylesheet" type="text/css">
        <?php echo $css_extend; ?>
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="wrapper">
            <?php $this->load->view('layout/topbar'); ?>
            <?php $this->load->view('layout/side-menu'); ?>