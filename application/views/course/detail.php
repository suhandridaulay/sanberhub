<?php $this->load->view('layout/header'); ?>
<?php 
$get_category = $this->CategoryModel->getId($course->catId);
?>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <?php $this->load->view('layout/topnav'); ?>
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h4 class="page-title title-inline"><?php echo $title; ?></h4>
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href=""><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item">Content</li>
                            <li class="breadcrumb-item active">Course</li>
                        </ol>
                    </div>
                </div>
            </div>

            <form action="<?php echo base_url('course/publish') ?>" id="course" method="post" enctype="multipart/form-data">
                <input type="hidden" id="csrf" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="id" value="<?php echo $course->courseId; ?>">
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-header">
                                <h4 class="card-title font-16 mt-0">Course Details</h4>
                            </div>
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <img src="<?php echo base_url('assets/uploads/course/'.$course->courseThumbnail); ?>" class="img-fluid mb-3">
                                        <div class="form-group">
                                            <label>Created at</label>
                                            <p><?php echo date('d/m/Y H:i',strtotime($course->created_date)); ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <p><?php echo $course->courseStatus == 'draft' ? '<span class="badge badge-pill badge-danger">Draft</span>' : '<span class="badge badge-pill badge-info">Publish</span>'; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <p><?php echo $course->courseTitle; ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Category</label>
                                            <p><?php echo $get_category->catName; ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Competencies</label>
                                            <p><?php echo $course->courseCompetencies; ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Caption</label>
                                            <p><?php echo $course->courseCaption; ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Update By</label>
                                            <p>Admin</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Updates</label>
                                            <p><?php echo date('d/m/Y H:i',strtotime($course->modified_date)); ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label>Privacy</label>
                                            <p><span class="badge badge-pill badge-danger"><?php echo ucwords($course->coursePrivacy); ?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            if($course->courseStatus == 'draft'){
                            ?>
                            <div class="card-footer">
                                <div class="float-right">
                                    <input type="submit" class="btn btn-danger" value="Publish Course" onclick="return confirm('Are you sure?')">
                                </div>
                            </div>
                            <?php 
                            }
                            ?>
                        </div>
                    </div> 
                </div> 
            </form>
        
        </div>
    </div>
</div>
<?php $this->load->view('layout/footer'); ?>