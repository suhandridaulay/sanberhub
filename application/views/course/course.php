<?php $this->load->view('layout/header'); ?>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <?php $this->load->view('layout/topnav'); ?>
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h4 class="page-title title-inline"><?php echo $title; ?></h4>
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href=""><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item">Content</li>
                            <li class="breadcrumb-item active">Course</li>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="row p-3">
                            <div class="col-md-6">
                                <h4 class="card-title font-16 mt-0">Course List</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="float-right">
                                    <a href="<?php echo base_url('course/add'); ?>" class="btn btn-outline-danger waves-effect waves-light"><i class="mdi mdi-plus"></i> Add Course</a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="table-info">
                                    <th width="50" class="text-center">#</th>
                                    <th>TITLE</th>
                                    <th width="150">ORGANIZATION</th>
                                    <th width="100" class="text-center">STATUS</th>
                                    <th width="150" class="text-center">LAST UPDATE</th>
                                    <th width="150" class="text-center">ACTION</th>
                                </thead>
                                <tbody>
                                    <?php 
                                    if($course){
                                        $no=1;
                                        foreach($course as $data){
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $no; ?></td>
                                        <td><?php echo $data->courseTitle; ?></td>
                                        <td><?php echo $data->orgName; ?></td>
                                        <td class="text-center"><?php echo $data->courseStatus == 'draft' ? '<span class="badge badge-pill badge-danger">Draft</span>' : '<span class="badge badge-pill badge-info">Publish</span>'; ?></td>
                                        <td class="text-center"><?php echo date('d/m/Y',strtotime($data->modified_date)); ?></td>
                                        <td class="text-center">
                                            <i class="mdi mdi-dots-vertical" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="<?php echo base_url('course/detail/'.$data->courseId); ?>" >Detail</a>
                                                <a class="dropdown-item" href="<?php echo base_url('course/delete/'.$data->courseId); ?>" onclick="return confirm('Are you sure you want to delete the data?');">Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php 
                                        $no++;
                                        }
                                    }else{
                                    ?>
                                    <tr>
                                        <td colspan="6" class="text-center">No Data</td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> 
            </div> 
        
        </div>
    </div>
</div>
<?php $this->load->view('layout/footer'); ?>