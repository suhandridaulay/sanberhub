<?php $this->load->view('layout/header'); ?>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <?php $this->load->view('layout/topnav'); ?>
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h4 class="page-title title-inline"><?php echo $title; ?></h4>
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href=""><i class="mdi mdi-home"></i></a></li>
                            <li class="breadcrumb-item">Content</li>
                            <li class="breadcrumb-item active">Course</li>
                        </ol>
                    </div>
                </div>
            </div>

            <form action="<?php echo base_url('course/store') ?>" id="course" method="post" enctype="multipart/form-data">
                <input type="hidden" id="csrf" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-header">
                                <h4 class="card-title font-16 mt-0">Add Course</h4>
                            </div>
                            <div class="card-body">
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Title <sup>*</sup></label>
                                            <input type="text" class="form-control" name="title" placeholder="Title" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Start Date <sup>*</sup></label>
                                            <input type="text" id="id_1" class="form-control" name="startdate" placeholder="Start Date" required>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Caption <sup>*</sup></label>
                                            <textarea class="form-control" name="caption" required></textarea>
                                        </div>
                                        <div>
                                        Content Category<span class="line-caption"></span>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Level <sup>*</sup></label>
                                            <select class="form-control" name="level" required>
                                                <option value=""></option>
                                                <option value="beginner">Beginner</option>
                                                <option value="medium">Medium</option>
                                                <option value="advanced">Advanced</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Categories <sup>*</sup></label>
                                            <select class="form-control" name="category" required>
                                                <option value=""></option>
                                                <?php 
                                                if($category){
                                                    foreach($category as $c){
                                                    ?>
                                                    <option value="<?php echo $c->catId; ?>"><?php echo $c->catName; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Competencies Based Category <sup>*</sup></label>
                                            <select class="form-control competencies" name="competencies[]" multiple>
                                            </select>
                                            <div class="text-muted mt-1">Separate with a comma (,)</div>
                                        </div>
                                        <div class="form-group">
                                            <label>Business Domain <sup>*</sup></label>
                                            <select class="form-control businessdomain" name="businessdomain[]" multiple>
                                            </select>
                                            <div class="text-muted mt-1">Separate with a comma (,)</div>
                                        </div>
                                       
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Thumbnails <sup>*</sup></label>
                                            <div class="custom-file">
                                                <input type="file" name="file" class="custom-file-input" onChange="showPreview(this);" accept="image/jpg,image/jpeg,image/png" required>
                                                <label class="custom-file-label">Choose file...</label>
                                            </div>
                                            <div id="targetLayer"></div>
                                        </div>

                                        
                                        <div class="form-group">
                                            <label>End Date <sup>*</sup></label>
                                            <input type="text" id="id_2" class="form-control" placeholder="End Date" name="enddate" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Description <sup>*</sup></label>
                                            <textarea class="form-control" name="description" required></textarea>
                                        </div>
                                        <div>
                                        Content Control<span class="line-caption"></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Privacy</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="privacy" id="public" value="public">
                                                <label class="form-check-label" for="public">
                                                    Public
                                                </label>
                                                <span>Everyone can watch your video</span>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="privacy" id="protected" value="protected" checked>
                                                <label class="form-check-label" for="protected">
                                                    Protected
                                                </label>
                                                <span>Everyone with link can watch your video</span>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="privacy" id="private" value="private">
                                                <label class="form-check-label" for="private">
                                                    Private
                                                </label>
                                                <span>Only someone with access can watch your video</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Sequence</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="sequence" id="random" value="random" >
                                                <label class="form-check-label" for="random">
                                                    Random
                                                </label>
                                                <span>User can select content without paying attention to the order of the content. (Course Flow)</span>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="sequence" id="sequence" value="sequence" checked>
                                                <label class="form-check-label" for="sequence">
                                                    Sequence
                                                </label>
                                                <span>User are required to follow the course flow.</span>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label>Certified</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="certified" id="certificate" value="y">
                                                <label class="form-check-label" for="certificate">
                                                    Certificate
                                                </label>
                                                <span>Everyone can watch your video</span>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="certified" id="nocertificate" value="n" checked>
                                                <label class="form-check-label" for="nocertificate">
                                                    No Certificate
                                                </label>
                                                <span>Everyone with link can watch your video</span>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label>Status <sup>*</sup></label>
                                            <select class="form-control" name="status" required>
                                                <option value="draft">Draft</option>
                                                <option value="publish">Publish</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Organization <sup>*</sup></label>
                                            <select class="form-control" name="organization" required>
                                                <option value=""></option>
                                                <?php 
                                                if($organization){
                                                    foreach($organization as $o){
                                                    ?>
                                                    <option value="<?php echo $o->orgId; ?>"><?php echo $o->orgName; ?></option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Learning Hours (on minutes) <sup>*</sup></label>
                                            <input class="form-control number" name="lhours" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <input type="submit" class="btn btn-danger" value="Add">
                                    <a href="<?php echo base_url('course'); ?>" class="btn btn-outline-danger waves-effect waves-light">Discard</a>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </form>
        
        </div>
    </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<script>
    function showPreview(objFileInput) {
        if (objFileInput.files[0]) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                $("#targetLayer").html('<img src="'+e.target.result+'" width="50%" class="mt-3" />');
                $(".icon-choose-image").css('opacity','0.5');
            }
            fileReader.readAsDataURL(objFileInput.files[0]);
        }
    }
    $(function() {
        $('.competencies').select2({
            tags: true,
            tokenSeparators: [','],
            allowClear: true,
        });
        $('.businessdomain').select2({
            tags: true,
            tokenSeparators: [','],
            allowClear: true,
        });

        $('#id_1').datetimepicker({
            "allowInputToggle": true,
            "showClose": true,
            "showClear": true,
            "showTodayButton": true,
            "format": "DD/MM/YYYY HH:mm:ss",
            icons: {
                time: 'fa fa-clock',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-calendar',
                clear: 'fa fa-trash',
                close: 'fa fa-check'
            }
        });

        $('#id_2').datetimepicker({
            "allowInputToggle": true,
            "showClose": true,
            "showClear": true,
            "showTodayButton": true,
            "format": "DD/MM/YYYY HH:mm:ss",
            icons: {
                time: 'fa fa-clock',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-calendar',
                clear: 'fa fa-trash',
                close: 'fa fa-check'
            }
        });

        $('input.number').keyup(function(event) {
            if(event.which >= 37 && event.which <= 40) return;
                $(this).val(function(index, value) {
                return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            });
        });
    });
</script>