<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('CourseModel');
    }

	public function index()
	{
		$css = '
        ';

        $js = '
        ';

        $data = [
            'title' => 'Course',
            'css_extend' => $css,
            'js_extend' => $js,
            'course' => $this->CourseModel->all()
        ];

		$this->load->view('course/course', $data);
	}

    public function add()
	{
        $this->load->model('CategoryModel');
        $this->load->model('OrganizationModel');

		$css = '
        <link href="'.base_url('assets/css/select2.css').'" rel="stylesheet" />
        <link href="'.base_url('assets/css/bootstrap-datetimepicker.min.css').'" rel="stylesheet" />
        ';

        $js = '
        <script src="'.base_url('assets/js/select2.min.js').'"></script>
        <script src="'.base_url('assets/js/moment-with-locales.min.js').'"></script>
        <script src="'.base_url('assets/js/bootstrap-datetimepicker.min.js').'"></script>
        ';

        $data = [
            'title' => 'Course',
            'css_extend' => $css,
            'js_extend' => $js,
            'category' => $this->CategoryModel->all(),
            'organization' => $this->OrganizationModel->all()
        ];

		$this->load->view('course/add', $data);
	}

    public function store(){
        $catid          = $this->input->post('category');
        $title          = trim($this->input->post('title'));
        $permalink      = Permalink($title);
        $caption        = trim($this->input->post('caption'));
        $startdate_ex   = explode(' ',$this->input->post('startdate'));
        $startdate_date = explode('/', $startdate_ex[0]);
        $startdate      = $startdate_date[2].'-'.$startdate_date[1].'-'.$startdate_date[0].' '.$startdate_ex[1];
        $enddate_ex     = explode(' ',$this->input->post('enddate'));
        $end_date_date  = explode('/', $enddate_ex[0]);
        $enddate        = $end_date_date[2].'-'.$end_date_date[1].'-'.$end_date_date[0].' '.$enddate_ex[1];
        $description    = trim($this->input->post('description'));
        $level          = $this->input->post('level');
        $competencies   = implode(',',$this->input->post('competencies'));
        $businessdomain = implode(',',$this->input->post('businessdomain'));
        $privacy        = $this->input->post('privacy');
        $sequence       = $this->input->post('sequence');
        $certified      = $this->input->post('certified');
        $status         = $this->input->post('status');
        $organization   = $this->input->post('organization');
        $lhours         = str_replace('.','',$this->input->post('lhours'));
        
 

        $namefile = '';
        if(isset($_FILES['file'])){
            $this->load->library('upload');
            $nmfile = base64_encode("course_".time());
            $config['upload_path'] = './assets/uploads/course/';
            $config['allowed_types'] = 'jpg|png|jpeg|bmp';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['file_name'] = $nmfile;

            $this->upload->initialize($config);

            if ($_FILES['file']['name']) {
                if ($this->upload->do_upload('file')) {
                    $gbr = $this->upload->data();
                    $namefile = $gbr['file_name'];
                }else{
                    //$error.=$this->upload->display_errors('', '<br>');
                }
            }
        }


        $field = array(
            'catId' => $catid,
            'orgId' => $organization,
            'courseTitle' => $title,
            'coursePermalink' => $permalink,
            'courseCaption' => $caption,
            'courseStartDate' => $startdate,
            'courseEndDate' => $enddate,
            'courseThumbnail' => $namefile,
            'courseDesc' => $description,
            'courseLevel' => $level,
            'courseCompetencies' => $competencies,
            'courseBusiness' => $businessdomain,
            'coursePrivacy' => $privacy,
            'courseSequence' => $sequence,
            'courseCertified' => $certified,
            'courseStatus' => $status,
            'courseLearning' => $lhours,
            'created_date' => date('Y-m-d H:i:s'),
            'modified_date' => date('Y-m-d H:i:s')
        );
        $save = $this->CourseModel->save($field);
        if($save){
            redirect('course');
        }
    }

    public function publish(){
        $id             = $this->input->post('id');

        $field = array(
            'courseStatus' => 'publish',
            'modified_date' => date('Y-m-d H:i:s')
        );
        $save = $this->CourseModel->update($field, $id);
        if($save){
            redirect('course');
        }
    }

    public function detail($id)
	{
        $this->load->model('CategoryModel');
        $this->load->model('OrganizationModel');

		$css = '
        <link href="'.base_url('assets/css/select2.css').'" rel="stylesheet" />
        <link href="'.base_url('assets/css/bootstrap-datetimepicker.min.css').'" rel="stylesheet" />
        ';

        $js = '
        <script src="'.base_url('assets/js/select2.min.js').'"></script>
        <script src="'.base_url('assets/js/moment-with-locales.min.js').'"></script>
        <script src="'.base_url('assets/js/bootstrap-datetimepicker.min.js').'"></script>
        ';

        $data = [
            'title' => 'Course',
            'css_extend' => $css,
            'js_extend' => $js,
            'course' => $this->CourseModel->getId($id)
        ];

		$this->load->view('course/detail', $data);
	}

    public function delete($id){
        if(!empty($id)){
            $delete = $this->CourseModel->delete($id);
        }
        redirect('course');
    }
}
