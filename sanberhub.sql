-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 13, 2022 at 11:12 AM
-- Server version: 5.7.34
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sanberhub`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `catId` int(11) NOT NULL,
  `catName` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`catId`, `catName`) VALUES
(1, 'Digital Content & Service');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `courseId` int(11) NOT NULL,
  `catId` int(11) NOT NULL,
  `orgId` int(11) NOT NULL,
  `courseTitle` varchar(200) NOT NULL,
  `coursePermalink` varchar(255) NOT NULL,
  `courseCaption` varchar(150) NOT NULL,
  `courseStartDate` datetime NOT NULL,
  `courseEndDate` datetime NOT NULL,
  `courseThumbnail` varchar(150) NOT NULL,
  `courseDesc` text NOT NULL,
  `courseLevel` enum('beginner','medium','advanced') NOT NULL,
  `courseCompetencies` text NOT NULL,
  `courseBusiness` text NOT NULL,
  `coursePrivacy` enum('public','protected','private') NOT NULL,
  `courseSequence` enum('random','sequence') NOT NULL,
  `courseCertified` enum('n','y') NOT NULL,
  `courseStatus` enum('draft','publish') NOT NULL,
  `courseLearning` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`courseId`, `catId`, `orgId`, `courseTitle`, `coursePermalink`, `courseCaption`, `courseStartDate`, `courseEndDate`, `courseThumbnail`, `courseDesc`, `courseLevel`, `courseCompetencies`, `courseBusiness`, `coursePrivacy`, `courseSequence`, `courseCertified`, `courseStatus`, `courseLearning`, `created_date`, `modified_date`) VALUES
(3, 1, 1, 'Introduction to Core Values AKHLAK', 'introduction-to-core-values-akhlak', 'Core Values BUMN - AKHLAK', '2022-11-13 17:28:45', '2022-11-19 17:31:50', 'Y291cnNlXzE2NjgzMzU1MTU.jpeg', 'Pengenalan Core VALUES BUMN', 'medium', 'Digital Product/Service Planning', 'Business Domain', 'protected', 'sequence', 'y', 'publish', 13500, '2022-11-13 17:31:55', '2022-11-13 18:05:23');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `orgId` int(11) NOT NULL,
  `orgName` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`orgId`, `orgName`) VALUES
(1, 'MY DIGILEARN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`catId`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`courseId`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`orgId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `catId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `courseId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `orgId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
